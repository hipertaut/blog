---
title: "Apa Itu Sistem Operasi?"
date: 2020-11-05T04:50:49+08:00
draft: true
---
Waktu kamu mau mengunduh program/aplikasi tertentu untuk komputermu, kamu mungkin disajikan beberapa versi dan diminta mengunduh yang sesuai sistem operasi yang kamu gunakan. Kalau aplikasi untuk desktop/laptop, biasanya ada versi Windows, MacOS atau GNU/Linux. Sedangkan untuk ponsel pintar ada versi Android atau iOS.

Mungkin kamu, seperti saya, pernah bertanya-tanya: apa itu sistem operasi? Apa fungsinya? Dsb. Semoga tulisan ini dapat membantu menjawabnya.

Pertama-tama, ada baiknya kita melihat susunan sistem komputer supaya lebih mudah memahami peran sistem operasi.

Secara sederhana, suatu sistem komputer terdiri atas empat komponen: pengguna, aplikasi, sistem operasi, dan perangkat keras. **Pengguna** itu adalah orang yang menggunakan komputer, contohnya saya dan kamu. **Perangkat keras** adalah perangkat-perangkat yang menyediakan sumber daya dasar untuk melakukan proses komputasi, contohnya: CPU, memori, dan perangkat input/output seperti monitor, mouse dan keyboard. **Aplikasi** adalah perangkat lunak yang kita gunakan untuk menyelesaikan pekerjaan tertentu dengan memanfaatkan perangkat keras, seperti Microsoft Word atau LibreOffice Writer untuk melakukan kerja olah kata. Dengan kata lain, aplikasi memuat cara-cara penggunaan perangkat keras untuk menunaikan fungsi tertentu. Di antara aplikasi dan perangkat keras, ada **sistem operasi**, suatu program yang mengatur dan mengoordinasikan penggunaan sumber daya di antara sejumlah aplikasi dan sejumlah pengguna.

Tujuan utama dari sistem komputer adalah menjalankan program tertentu untuk memudahkan pengguna melakukan kegiatan tertentu. Untuk itu, dibuatlah _hardware_ komputer. Karena menggunakan hardware aja itu ribet, maka dikembangkanlah aplikasi. Karena aplikasi-aplikasi itu memiliki sejumlah operasi yang serupa — seperti mengendalikan perangkat input/output, kemudian dibuatlah sistem operasi — **_software_ yang menjalankan fungsi antarmuka, pengendalian dan pengalokasian sumber daya untuk pengguna dan aplikasi.[1]**

Lebih jauh, kita bisa melihat peran sistem operasi dari dua sudut pandang: pengguna dan sistem.

## Sudut Pandang Pengguna[2]

Dari sudut pandang pengguna, sistem operasi berfungsi untuk memudahkan dan/atau memaksimalkan penggunaan perangkat keras. Sudut pandang pengguna ini bisa berbeda-beda untuk tiap pengguna, tergantung dari antarmuka atau jenis komputer yang mereka gunakan.

Ada orang yang menggunakan PC, yang terdiri dari unit sistem, monitor, _mouse_ dan _keyboard_. Dalam jenis komputer ini, biasanya sistem operasi mengutamakan kemudahan penggunaan perangkat keras dari PC tersebut untuk menunjang kegiatan si pengguna ketimbang pemanfaatan sumberdaya — bagaimana perangkat lunak dan keras dibagi untuk lebih dari satu pengguna. Dengan kata lain, sumber daya dalam komputer tersebut diutamakan untuk performa maksimal bagi pengguna tunggal.

Sedangkan dalam kasus lain, misalnya ketika kamu menggunakan komputer _mainframe_ yang bisa diakses banyak pengguna, sistem operasi yang digunakan biasanya mengutamakan pemanfaatan sumber daya ketimbang kemudahan penggunaan. Di sini, sistem operasi mengatur efisiensi penggunaan sumber daya untuk memastikan setiap pengguna tidak menggunakan sumber daya yang tersedia melebihi pembagian yang telah ditetapkan sebelumnya.

Dalam kasus lainnya lagi, kamu mungkin menggunakan _workstation_ yang terhubung pada _server._ _Server_ itu juga terhubung pada _workstation_ lainnya. Di sini, sistem operasi menyeimbangkan penggunaan individu dan pemanfaatan sumber daya.

## Sudut Pandang Sistem[3]

Dari sudut pandang sistem komputer — yang terdiri dari sejumlah perangkat lunak dan keras, sistem operasi berperan **mengalokasikan sumber daya** untuk program dan pengguna yang spesifik. Alokasi ini dilakukan secara efisien dan adil, guna mencegah permintaan penggunaan sumber daya yang bertentangan.

Sistem operasi juga bisa dipandang sebagai program kontrol yang mengatur eksekusi aplikasi agar tidak terjadi _error_.

## Sistem Operasi Terdiri dari Apa Saja?

Kalau melihat sistem operasi terdiri dari apa saja, kita sulit mendefinisikan sistem operasi, sebab setiap sistem operasi isinya beda-beda. Kalau kita bilang: “Semua sistem operasi itu punya tampilan antarmuka grafis,” maka sistem operasi yang tidak menyertakan GUI secara _default_ seperti Arch Linux tidak masuk dalam kategori sistem operasi.

Dalam penggunaannya sehari-hari, biasanya istilah “sistem operasi” merujuk pada sekumpulan _software_ yang diberikan oleh vendor/pengembang saat kita membeli/meminta sistem operasi tersebut. Contohnya saat kamu membeli Microsoft Windows 8, kamu mendapatkan satu software yang terdiri dari kompilator, kernel, antarmuka pengguna, dsb.

---

**Catatan Kaki:**

[1] Silberschatz dkk, 2005, hal. 6

[2] _Ibid,_ hal. 4-5

[3] _Ibid,_ hal. 3

---

**Sumber:**

Silberschatz, A., Galvin, P. B., dan Gagne, G. 2005. _Operating System Concepts_ (ed. 7). USA: John Wiley & Sons, Inc.
