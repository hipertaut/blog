---
title: "Apa Arti Istilah 'Digital' dalam Komputer?"
date: 2020-10-29T04:50:15+08:00
draft: true
---
Mungkin kamu udah punya bayangan arti istilah “digital” atau “media digital” itu apa. Lebih jauh, mungkin kamu udah punya definisi untuk istilah itu. Kebetulan waktu lagi ngerjain tugas, saya nemu penjelasan tentang apa itu “digital” dan kepingin berbagi dengan pembaca lewat tulisan ini.

Kalau dari akar katanya, “digit,” yang berarti angka, kita langsung dapat bayangan kalau istilah “digital” berarti berhubungan dengan angka-angka. Tapi kalau bicara soal komputer, misalnya, bagian mana sih dari kerja alatnya yang berhubungan dengan angka-angka?

Supaya lebih mudah memahaminya, kita bisa membandingkan “media digital” dengan “media analog” bekerja.

Kita ambil contoh menulis surat cinta di buku catatan (analog) dan aplikasi olah kata di laptop (digital). Waktu kita menulis di atas kertas, kita mengubah pesan dalam kepala kita menjadi bentuk simbol-simbol visual yang terbuat dari tinta dan menyimpannya di permukaan kertas. Ada objek yang disimpan ke dalam objek lain dan melibatkan perubahan secara fisik dan kimiawi.

Sedangkan kalau kita menggunakan aplikasi pengolah kata di komputer, kita menekan tombol di _keyboard,_ komputer _(hardware)_ kita akan menerima sinyal listrik tertentu, lalu menerjemahkannya ke dalam “bahasa” yang dimengerti oleh si aplikasi _(software)_. Berikutnya, aplikasi tersebut akan mengirimkan sinyal balik agar komputer _(hardware)_ kita tahu apa _output_ yang mesti ditampilkan ke kita — dalam contoh ini, adalah menampilkan kata yang tadi _input_ pada layar.

_Hardware_ komputer kita itu pada dasarnya cuma mengerti kondisi kelistrikan nyala atau hidup, yang diberi nilai 0 atau 1. Jadi digital dalam komputer itu berarti penyematan nilai angka yang merepresentasikan fenomena kelistrikan tertentu. Waktu kita menulis kata “sayang,” misalnya, komputer akan memberikan nilai 0 dan 1 yang berarti ada bagian yang “menyala” dan ada yang tetap mati atau dimatikan.

Apakah angka yang digunakan harus selalu 0 dan 1? Sebenernya sih enggak. Angka itu kan merujuk ke kondisi tertentu. Bisa aja kita pakai angka 0-9. Tapi konsekuensinya: kita harus menyiapkan sembilan sepuluh kondisi untuk masing-masing angka itu. Penggunaan dua kondisi saja dipilih karena (katanya) lebih mudah dikombinasikan dan lebih murah.

---

**Sumber:**  
Lister, M., Dovey, J., Giddings, S., Kelly, K. dan Grant, I., 2009. [_New Media: A Critical Introduction_ (ed. 2).](https://books.google.co.id/books?id=gMx-AMRg3A0C&printsec=frontcover&dq=new+media+a+critical+introduction+2nd+edition&hl=en&sa=X&ved=2ahUKEwif9pryuNnsAhVUjuYKHaDKCVQQ6AEwAHoECAEQAg) London dan New York: Routledge.
